﻿using AlintaCodeTest.BusinessLogic.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;

namespace AlintaCodeTest.BusinessLogic.Tests.FakeServices
{
    public class FakeWebService : IWebService
    {
        public async Task<T> Get<T>(string url)
        {
            StorageFolder folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            folder = await folder.GetFolderAsync("Resources");
            StorageFile file = await folder.GetFileAsync("FakeData.json");

            using (var reader = new StreamReader(await file.OpenStreamForReadAsync()))
            {
                var content = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(content);
            }
        }
    }
}
