﻿using AlintaCodeTest.BusinessLogic.Repositories;
using AlintaCodeTest.BusinessLogic.Repositories.Interfaces;
using AlintaCodeTest.BusinessLogic.Services.Interfaces;
using AlintaCodeTest.BusinessLogic.Tests.FakeServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace AlintaCodeTest.BusinessLogic.Tests.Repositories
{
    [TestClass]
    public class ActorsRepositoryTests
    {
        private const string JohnBelushiName = "John Belushi";

        private static IUnityContainer _container = new UnityContainer();
        private static IActorsRepository _actorsRepository;

        [ClassInitialize]
        public static void SetupTests(TestContext context)
        {
            _container.RegisterType<IWebService, FakeWebService>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IActorsRepository, ActorsRepository>(new ContainerControlledLifetimeManager());

            _actorsRepository = _container.Resolve<IActorsRepository>();
        }


        [TestMethod]
        public async Task TestCorrectNumberOfActorsReturned()
        {
            var actors = await _actorsRepository.GetRolesGroupedByActors();

            // 9 actors + Roles without an actor group
            Assert.AreEqual(10, actors.Count());
        }


        [TestMethod]
        public async Task TestCorrectOrderOfActorRoles()
        {
            var actors = await _actorsRepository.GetRolesGroupedByActors();

            var johnBelushi = actors.Where(a => JohnBelushiName.Equals(a.Name))
                .FirstOrDefault();

            Assert.AreEqual(johnBelushi.Roles[0], "John Blutarsky");
            Assert.AreEqual(johnBelushi.Roles[1], "Jake Blues");
        }


    }
}
