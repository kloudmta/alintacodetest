﻿using AlintaCodeTest.BusinessLogic.Services;
using AlintaCodeTest.BusinessLogic.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Unity;

namespace AlintaCodeTest.BusinessLogic.Tests.Services
{
    [TestClass]
    public class WebServiceTests
    {
        private const string alintaUrl = "https://alintacodingtest.azurewebsites.net/api/movies";
        private static IUnityContainer _container = new UnityContainer();

        [ClassInitialize]
        public static void SetupTests(TestContext context)
        {
            _container.RegisterType<IWebService, WebService>();
        }

        [TestMethod]
        public async Task GetMethodReturnsObjectTest()
        {
            var webService = _container.Resolve<IWebService>();

            dynamic result = await webService.Get<dynamic>(alintaUrl);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count > 0);
        }
    }
}
