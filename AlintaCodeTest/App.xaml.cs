﻿using AlintaCodeTest.BusinessLogic.Repositories;
using AlintaCodeTest.BusinessLogic.Repositories.Interfaces;
using AlintaCodeTest.BusinessLogic.Services;
using AlintaCodeTest.BusinessLogic.Services.Interfaces;
using AlintaCodeTest.Services;
using Prism.Mvvm;
using Prism.Windows;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml.Navigation;

namespace AlintaCodeTest
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : PrismApplication
    {
        // Unity dependency injection contianer to register and resolve types and instances
        private readonly IUnityContainer _container = new UnityContainer();
        public IUnityContainer Container => _container;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
        }


        protected override Task OnInitializeAsync(IActivatedEventArgs args)
        {
            _container.RegisterInstance(NavigationService);
            _container.RegisterInstance(SessionStateService);

            // Register Services
            _container.RegisterType<IWebService, WebService>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IDialogService, DialogService>(new ContainerControlledLifetimeManager());

            // Register Repositories
            _container.RegisterType<IActorsRepository, ActorsRepository>(new ContainerControlledLifetimeManager());

            SetupViewModelLocator();

            return base.OnInitializeAsync(args);
        }


        private void SetupViewModelLocator()
        {
            // Set ViewModelLocator AutoWire path to AlintaCodeTest.BusinessLogic solution
            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver((viewType) =>
            {
                var viewModelTypeName = string.Format(CultureInfo.InvariantCulture, "AlintaCodeTest.BusinessLogic.ViewModels.{0}ViewModel, AlintaCodeTest.BusinessLogic", viewType.Name);
                var viewModelType = Type.GetType(viewModelTypeName);
                return viewModelType;
            });
        }


        protected override Task OnLaunchApplicationAsync(LaunchActivatedEventArgs args)
        {
            // Entry point: go to schedule page
            NavigationService.Navigate("Start", null);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }


        /// <summary>
        /// Resolve unity container view model types when using constructor dependency injection
        /// </summary>
        protected override object Resolve(Type type)
        {
            return _container.Resolve(type);
        }
    }
}
