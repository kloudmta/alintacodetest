﻿using Prism.Windows.Mvvm;

namespace AlintaCodeTest.Views.Common
{
    /// <summary>
    /// Page base to be used for all pages in the AlintaCodeTest project
    /// </summary>
    public abstract class PageBase : SessionStateAwarePage
    {

    }
}
