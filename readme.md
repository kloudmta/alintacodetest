# Alinta Code Test

# Minimum Requirements
Computer should have Windows 10 Anniversary Update (10.0; Build 14393) installed.


# Assumptions made

1. No requirement was defined for how the actors should be sorted so they are being sorted by name
2. Roles without an actor are placed under a "Roles without an Actor" header at the very bottom of the list (not included in sorting mentioned above)
3. Roles from a movie without a name will be placed before all other roles for an actor in no particular order if there are more than one
4. It is assumed that a role's name is unique and will not reoccur in other movies. If a duplicate role is found, only the role from the first movie (sorted alphabetically) is added. Subsequent duplicates are not added.


# Running instructions

## Running the solution
1. Ensure that you have Visual Studio 2017 installed on a Windows Machine
2. Ensure that you have necessary UWP development components installed
3. Set **AlintaCodeTest (Universal Windows)** as the start up project
4. Run the solution

## Installing the solution via package in Windows 10 Developer mode

1. Ensure that you have windows developer mode enabled on your computer
2. Open the solution root in file explorer
3. Extract AlintaCodeTest_InstallPackage.zip 
4. Open the extracted folder and right click *Add-AppDevPackage.ps1*
5. Select *Run with PowerShell*
6. Follow the on screen instructions
7. Open the AlintaCodeTest application from the Windows Start Menu

# Contact

For any further questions, please contact Mahmoud Abduo at mahmoud.abduo@kloud.com.au
