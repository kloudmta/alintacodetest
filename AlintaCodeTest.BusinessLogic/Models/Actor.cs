﻿using System.Collections.Generic;

namespace AlintaCodeTest.BusinessLogic.Models
{
    /// <summary>
    /// Model to represent an Actor and the name of roles they played
    /// </summary>
    public class Actor
    {
        public Actor(string name)
        {
            this.Name = name;
        }

        public string Name { get; private set; }
        public List<string> Roles { get; set; } = new List<string>();


        /// <summary>
        /// Adds a role to this Actor's roles if it hasn't been added already
        /// </summary>
        /// <param name="newRole">the new role to be added</param>
        public void AddRole(string newRole)
        {
            if (!Roles.Contains(newRole))
            {
                Roles.Add(newRole);
            }
        }
    }
}
