﻿namespace AlintaCodeTest.BusinessLogic.Models
{
    /// <summary>
    /// Model that represents a role in a movie and the actor
    /// that played that rol
    /// </summary>
    public class Role
    {
        public string Name { get; set; }
        public string Actor { get; set; }
    }
}
