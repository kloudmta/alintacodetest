﻿namespace AlintaCodeTest.BusinessLogic.Models
{
    /// <summary>
    /// Model representing a movie and the roles in that movie
    /// </summary>
    public class Movie
    {
        public string Name { get; set; }
        public Role[] Roles { get; set; }
    }
}
