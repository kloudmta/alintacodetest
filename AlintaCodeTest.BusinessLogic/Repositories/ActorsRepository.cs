﻿using AlintaCodeTest.BusinessLogic.Models;
using AlintaCodeTest.BusinessLogic.Repositories.Interfaces;
using AlintaCodeTest.BusinessLogic.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlintaCodeTest.BusinessLogic.Repositories
{
    public class ActorsRepository : IActorsRepository
    {
        private const string MoviesEndPoint = "https://alintacodingtest.azurewebsites.net/api/movies";
        private const string RolesWithoutAnActorKey = "Roles without an Actor";

        private readonly IWebService _webService;

        public ActorsRepository(IWebService webService)
        {
            _webService = webService;
        }

        /// <summary>
        /// Gets a list of actors with their roles sorted by the name of the movie that 
        /// role was in
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Actor>> GetRolesGroupedByActors()
        {
            var actors = new SortedDictionary<string, Actor>();
            var rolesWithoutAnActor = new Actor(RolesWithoutAnActorKey);

            var movies = await _webService.Get<IEnumerable<Movie>>(MoviesEndPoint);

            // Sometimes API returns an empty response
            if (movies == null) return new List<Actor>();

            // Sort movies by name
            movies = movies.OrderBy(m => m.Name)
                .Select(m => m);

            foreach (var movie in movies)
            {
                foreach (var role in movie.Roles)
                {
                    var actorKey = role.Actor;

                    if (string.IsNullOrWhiteSpace(actorKey))
                    {
                        rolesWithoutAnActor.AddRole(role.Name);
                        continue;
                    }

                    if (actors.ContainsKey(actorKey))
                    {
                        actors[actorKey].AddRole(role.Name);
                    }
                    else
                    {
                        actors[actorKey] = new Actor(role.Actor);
                        actors[actorKey].AddRole(role.Name);
                    }
                }
            }

            var resultList = actors.Values.ToList();

            // Append roles without an actor defined to the very bottom of the list
            if (rolesWithoutAnActor.Roles.Count > 0)
            {
                resultList.Add(rolesWithoutAnActor);
            }

            return resultList;
        }

    }
}
