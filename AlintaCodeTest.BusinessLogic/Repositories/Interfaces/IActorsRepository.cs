﻿using AlintaCodeTest.BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlintaCodeTest.BusinessLogic.Repositories.Interfaces
{
    public interface IActorsRepository
    {
        /// <summary>
        /// Gets a list of actors with their roles sorted by the name of the movie that 
        /// role was in
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Actor>> GetRolesGroupedByActors();
    }
}
