﻿using Prism.Windows.Mvvm;

namespace AlintaCodeTest.BusinessLogic.ViewModels.Common
{
    /// <summary>
    /// AlintaCodeTest View Model Base
    /// </summary>
    public abstract class ActViewModelBase : ViewModelBase
    {

    }
}
