﻿using AlintaCodeTest.BusinessLogic.Models;
using AlintaCodeTest.BusinessLogic.Repositories.Interfaces;
using AlintaCodeTest.BusinessLogic.Services.Interfaces;
using AlintaCodeTest.BusinessLogic.ViewModels.Common;
using Prism.Commands;
using System;
using System.Collections.ObjectModel;
using System.Net;

namespace AlintaCodeTest.BusinessLogic.ViewModels
{
    /// <summary>
    /// View Model for start page
    /// </summary>
    public class StartPageViewModel : ActViewModelBase
    {
        private readonly IActorsRepository _actorsRepository;
        private readonly IDialogService _dialogService;

        private ObservableCollection<Actor> _actors;
        private bool _isLoading;

        public StartPageViewModel(IActorsRepository actorsRepository,
            IDialogService dialogService)
        {
            _actorsRepository = actorsRepository;
            _dialogService = dialogService;

            Actors = new ObservableCollection<Actor>();

            LoadActors();
        }

        public DelegateCommand RefreshActorsCommand => new DelegateCommand(RefreshActors);

        /// <summary>
        /// The list of actors that should be displayed by the corresponding view
        /// </summary>
        public ObservableCollection<Actor> Actors
        {
            get { return _actors; }
            set { SetProperty(ref _actors, value); }
        }


        /// <summary>
        /// Whether the StartPageViewModel is loading something or not
        /// </summary>
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }


        /// <summary>
        /// Load actors from repository and insert them into Actors observable collection
        /// </summary>
        private async void LoadActors()
        {
            IsLoading = true;

            try
            {
                var actors = await _actorsRepository.GetRolesGroupedByActors();

                foreach (var actor in actors)
                {
                    Actors.Add(actor);
                }
            }
            catch (WebException we)
            {
                _dialogService.ShowAlert("An error occurred.",
                    "An error occurred while communicating with the server. Please check your internet connection.");
            }
            catch (Exception e)
            {
                _dialogService.ShowAlert("An unknown error occurred.",
                    "Unable to load actors, please try again");
            }

            IsLoading = false;
        }


        /// <summary>
        /// Refreshes the list of actors loaded
        /// </summary>
        private void RefreshActors()
        {
            Actors.Clear();
            LoadActors();
        }

    }
}
